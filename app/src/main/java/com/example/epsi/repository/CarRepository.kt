package com.example.epsi.repository

import com.example.epsi.db.entity.CarEntity
import kotlinx.coroutines.flow.Flow

interface CarRepository {
    fun getCars(): Flow<List<CarEntity>>

    suspend fun addNewCar(carName: String)
}
