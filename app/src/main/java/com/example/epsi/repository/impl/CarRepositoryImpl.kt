package com.example.epsi.repository.impl

import android.content.Context
import com.example.epsi.db.ProjectDatabase
import com.example.epsi.db.entity.CarEntity
import com.example.epsi.repository.CarRepository
import kotlinx.coroutines.flow.Flow

class CarRepositoryImpl(context: Context) : CarRepository {

    private val db = ProjectDatabase.getInstance(context)
    private val carDao = db.carDao()

    override fun getCars(): Flow<List<CarEntity>> = carDao.getCars()

    override suspend fun addNewCar(carName: String) {
        carDao.insert(CarEntity(carName))
    }
}
