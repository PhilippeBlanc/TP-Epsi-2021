package com.example.epsi.core

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.epsi.ui.garage.GarageViewModel

class ViewModelFactory(private val application: Application) : ViewModelProvider.AndroidViewModelFactory(application) {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(GarageViewModel::class.java) -> {
                GarageViewModel(application) as T
            }
            else -> {
                super.create(modelClass)
            }
        }
    }
}