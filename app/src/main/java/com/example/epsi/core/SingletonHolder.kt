package com.example.epsi.core

open class SingletonHolder<T, A>(creator: (A) -> T) {
    private var creator: ((A) -> T)? = creator
    @Volatile private var instance: T? = null

    fun getInstance(arg: A): T {
        val instance1 = instance
        if (instance1 != null) {
            return instance1
        }

        return synchronized(this) {
            val instance2 = instance
            if (instance2 != null) {
                instance2
            } else {
                val created = creator!!(arg)
                instance = created
                creator = null
                created
            }
        }
    }
}