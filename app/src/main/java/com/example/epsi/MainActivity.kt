package com.example.epsi

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.epsi.databinding.ActivityMainBinding
import com.example.epsi.ui.garage.GarageActivity

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.mainActivityGoToGarage.setOnClickListener {
            GarageActivity.startActivity(this)
        }
    }
}
