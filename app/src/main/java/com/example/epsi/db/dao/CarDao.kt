package com.example.epsi.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.epsi.db.entity.CarEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface CarDao : BaseDao<CarEntity> {

    @Query("SELECT * FROM car")
    fun getCars(): Flow<List<CarEntity>>

    @Query("DELETE FROM car WHERE id = :carId")
    suspend fun deleteCarById(carId: Int)
}
