package com.example.epsi.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.epsi.core.SingletonHolder
import com.example.epsi.db.ProjectDatabase.Companion.DB_NAME
import com.example.epsi.db.dao.CarDao
import com.example.epsi.db.entity.CarEntity

@Database(
    entities = [
        CarEntity::class
    ],
    version = 1,
    exportSchema = false
)
abstract class ProjectDatabase : RoomDatabase() {

    abstract fun carDao(): CarDao

    companion object : SingletonHolder<ProjectDatabase, Context>({
        Room.databaseBuilder(it, ProjectDatabase::class.java, DB_NAME).build()
    }) {
        private const val DB_NAME = "ProjectDatabase"
    }
}
