package com.example.epsi.ui.garage

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.epsi.core.ViewModelFactory
import com.example.epsi.databinding.ActivityGarageBinding

class GarageActivity : AppCompatActivity() {

    companion object {
        fun startActivity(activity: Activity) {
            val intent = Intent(activity, GarageActivity::class.java)
            activity.startActivity(intent)
        }
    }

    private lateinit var binding: ActivityGarageBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGarageBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val factory: ViewModelProvider.Factory = ViewModelFactory(application)
        val garageViewModel = ViewModelProvider(this, factory).get(GarageViewModel::class.java)

        val carAdapter = CarAdapter(listOf())
        binding.garageActivityCarRecycler.adapter = carAdapter
        binding.garageActivityCarRecycler.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        garageViewModel.getMyCars().observe(
            this,
            Observer { cars ->
                carAdapter.updateCars(cars)
            }
        )

        binding.garageActivityAddCarBtn.setOnClickListener {
            if (binding.garageActivityCarName.text.isNotEmpty()) {
                garageViewModel.addCar(binding.garageActivityCarName.text.toString())
            }
        }
    }
}
