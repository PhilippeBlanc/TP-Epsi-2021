package com.example.epsi.ui.garage.helper

import com.example.epsi.R

object CarHelper {

    private val carImages = listOf(
        R.drawable.car_1,
        R.drawable.car_2,
        R.drawable.car_3,
        R.drawable.car_4
    )

    fun getCarAtPosition(position: Int): Int {
        val listSize = carImages.size
        val index = position.rem(listSize)
        return carImages[index]
    }
}
