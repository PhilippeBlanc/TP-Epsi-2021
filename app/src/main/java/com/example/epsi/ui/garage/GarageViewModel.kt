package com.example.epsi.ui.garage

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.epsi.repository.CarRepository
import com.example.epsi.repository.impl.CarRepositoryImpl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class GarageViewModel(context: Context) : ViewModel() {

    private val carRepository: CarRepository = CarRepositoryImpl(context)

    fun getMyCars(): LiveData<List<String>> = carRepository.getCars()
        .map { cars -> cars.map { it.name } }
        .asLiveData()

    fun addCar(carName: String) {
        viewModelScope.launch(Dispatchers.IO) {
            carRepository.addNewCar(carName)
        }
    }
}
