# todo 1.1 : item_car : Ajouter une icone de suppression à droite du cartouche
#      télécharger une icone depuis https:#fonts.google.com/ format Web / SVG
#      Ajouter l'image dans les Drawable en créant un nouveau VectorAsset
# todo 1.2 : CarAdapter.CarViewHolder Ajouter une méthode onBind, à appeler depuis onBindViewHolder en lui passant le modèle car en param
#      reporter l'affichage du text dans la nouvelle méthode
# todo 1.3 : CarAdapter : créer l'interface CarAdapterListerner avec la méthode delete car avec carId: Int comme paramètre
# todo 1.4 : CarAdapter : transmettre la property "val listener: CarAdapterListerner" via le constructeur de la classe
# todo 1.5 : GarageActivity : passer "this" en paramètre pour le listener de l'adapter et laisser la classe implémenter CarAdapterListener avec une méthode qui présente un toast
# todo 1.6.1 : créer une classe CarModel avec un id et un label (dans le dossier ui.garage).
# todo 1.6.2 : GarageViewModel : transformer la méthode "getMyCars" pour remplacer la list<String> par une List<CarModel>
# todo 1.6.3 : redescendre jusqu'à CarAdapter pour que la prop. cars soit une liste de <CarModel> au lieu d'une liste de <String>

# todo 1.7 : CarViewHolder. dans la méthode onBind, au click sur l'icone de suppression appeler notre listener pour car.id
#      vérifier dans l'application le toast affiche bien l'id de l'objet Car sur lequel j'ai clické.

# todo 1.8 : Remplcer le toast en appelant garageViewModel.deleteCar(carId), créer la méthode dans le viewModel qui supprimera l'élément depuis son id.
#      wrapper l'appel dans un viewModelScope{} pour utiliser un coroutine scope (gestion de l'asynchronisme)
#      créer dans CarRepository "suspend fun deleteCarById(id: Int)"
#      implémenter la méthode en appelant carDao.deleteCarById

# todo 2.0 - on va rajouter la marque depuis une liste prédéfinie 
# todo 2.1 : activity_garage.xml :créer un élément Spinner au dessus de l'edit text, d'id garage_activity_car_brand
# todo 2.2: strings.xml : créer une resource de type string-array, ayant pour name "car_brand_array", et déclarer les marques dans des items "<item>Brand</item>"   -->
# todo 2.3 : GarageActivity : remplir le spinner avec nos resources.
#      ArrayAdapter.createFromResource(context, array resource id, android.R.layout.simple_spinner_item)
# todo 2.4 : GarageActivity : au click sur le bouton d'atout, récupérer la valeur du spinner, et la transmettre dans la méthode addCar en rajoutant un param. brand
# todo 2.5 : GarageViewModel : modifier la methode addNewCar, pour envoyer le paramètre brand: String
# todo 2.6 : CarRepository : modifier CarEntity pour le construire avec le paramètre brand et name
# todo 2.7.0 : Attention : une entité est une table sql, nous allons donc devoir mettre à jour notre base de données
# todo 2.7.1 : Dans ProjectDatabase, incrémenter le numéro de version dans l'annotation @Database
# todo 2.7.2 : Toujours dans ProjectDatabase, rajouter l'instruction ".fallbackToDestructiveMigration()" dans le builder
#                Cette instruction détruira la base de données lorsque l'application fera la montée de version de la bdd
#                Il est bien sur possible de créer des scripts de migration au lieu de faire une destruction
# todo 2.8 : créer dans le repertoire ui.garage une data class CarModel, avec un name et une brand.
# todo 2.9 : modifier la méthode getMyCars pour retourner LiveData<List<CarModel>>
# todo 2.10 : modifier carAdapter pour que cars soit une List<CarModel>
# todo 2.11 : item_car.xml : Ajouter un textView pour afficher car brand
# todo 2.12 : CarAdapter.CarViewHolder : afficher la property brand dans le textView créé dans la méthode onBind.

# On va maintenant visualiser noter garage en détails
# todo 3.1.1 : ajouter dans le layout activity_main un textview main_activity_go_to_garage_details
# todo 3.1.2 : au click sur main_activity_go_to_garage_details ouvrir l'activité GarageDetailsActivity

# Depuis la classe GarageDetailActivity
# todo 3.2.1 : créer un fragment CarDetailFragment avec 2 élements textView et 1 ImageView (disposition à votre gout)
#              servez vous des tools:text= et tools:src="@sample/..." pour visualiser le résultat
# todo 3.2.2 : Dans le layout de l'activité ajouter un composant ViewPager2, qui prend la taille du parent
# todo 3.2.3 : créer une classe CarDetailAdapter qui implémente FragmentStateAdapter : CarDetailAdapter(fragmentActivity: FragmentActivity) : FragmentStateAdapter(fragmentActivity)
# todo 3.2.4 : dans CarDetailAdapter créer la property var cars : List<CarModel>, initialisé avec une liste vide
# todo 3.2.4 : remplir la méthode getItemCount en retournant la taille de la liste
# todo 3.2.5 : remplir la méthode createFragment en créant le fragment CarDetailFragment
# todo 3.2.6 : ajouter une méthode setCar(car: CarModel) à votre fragment, et setter vos textes et une image ()
#               récupérer une image aléatoirement avec la méthode val imageResourceId = CarHelper.getCarAtPosition(position)
#               setImageDrawable(ContextCompat.getDrawable(requireContext(), imageResourceId))
# todo pbc : fournir le random de l'image
# todo 3.2.7 : créer une propriété de classe carDetailAdapter, et implémenter notre CarDetailAdapter
# todo 3.2.8 : fournir l'adpater au ViewPager
# todo 3.2.8 : utiliser garageViewModel pour "observer" nos objets CarModel, et appeler carDetailAdatpter.setCars() ;
# todo 3.2.9 : implémenter la méthode et setter la liste de l'adapter


    
        